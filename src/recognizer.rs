use crate::grammar::Rule;
use crate::grammar::Symbol;

use std::collections::HashMap;
use std::collections::HashSet;

use std::process;

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct EarleyItem<'a, T> {
    pub rule: &'a Rule<T>,
    pub dot: usize,
    pub start: usize,
}

#[derive(Debug, PartialEq, Clone)]
pub struct EarleySet<'a, T> {
    pub values: Vec<EarleyItem<'a, T>>,
}

impl<'a, T: PartialEq> EarleySet<'a, T> {
    fn append(&mut self, v: EarleyItem<'a, T>) {
        if !self.values.contains(&v) {
            self.values.push(v);
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Chart<'a, T> {
    pub items: Vec<EarleySet<'a, T>>,
    predict_lut: HashMap<String, Vec<&'a Rule<T>>>,
    first_set: HashMap<String, Vec<&'a T>>,
    nullable: HashSet<String>,
    index: usize,
}

pub fn new<T>(grammar: &[Rule<T>]) -> Chart<'_, T> {
    let mut chart = Chart {
        items: Vec::new(),
        predict_lut: HashMap::new(),
        first_set: HashMap::new(),
        nullable: HashSet::new(),
        index: 0,
    };

    for r in grammar.iter() {
        chart
            .predict_lut
            .entry(r.lhs.lhs())
            .or_insert_with(Vec::new)
            .push(&r);
    }

    let mut nullable_size = 0;
    let mut old_nullable_size = 1;

    while old_nullable_size != nullable_size {
        old_nullable_size = nullable_size;

        for r in grammar.iter() {
            let mut i = 0;

            while !chart.nullable.contains(&r.lhs.lhs()) {
                if r.rhs.is_empty() {
                    chart.nullable.insert(r.lhs.lhs());
                    nullable_size += 1;
                } else if let Some(Symbol::Nonterminal(v)) = r.rhs.get(i) {
                    if chart.nullable.contains(v) {
                        i += 1;
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }
        }
    }

    let mut keep_going = false;
    while keep_going {
        keep_going = false;

        for r in grammar.iter() {
            let old_size = chart
                .first_set
                .entry(r.lhs.lhs())
                .or_insert_with(Vec::new)
                .len();
            for s in r.rhs.iter() {
                if let Symbol::Terminal(t) = s {
                    chart
                        .first_set
                        .entry(r.lhs.lhs())
                        .or_insert_with(Vec::new)
                        .push(t);
                    break;
                } else if let Symbol::Nonterminal(nt) = s {
                    let xs = chart
                        .first_set
                        .entry(nt.to_string())
                        .or_insert_with(Vec::new)
                        .clone();
                    for x in xs.iter() {
                        chart
                            .first_set
                            .entry(r.lhs.lhs())
                            .or_insert_with(Vec::new)
                            .push(x);
                    }

                    if !chart.nullable.contains(nt) {
                        break;
                    }
                }
            }

            if old_size
                == chart
                    .first_set
                    .entry(r.lhs.lhs())
                    .or_insert_with(Vec::new)
                    .len()
            {
                keep_going = true;
            }
        }
    }

    let top = EarleyItem {
        rule: &grammar[0],
        dot: 0,
        start: 0,
    };

    chart.items.push(EarleySet { values: Vec::new() });
    chart.items[0].values.push(top);

    chart
}

impl<'a, T: PartialEq> Chart<'a, T> {
    pub fn consume(&mut self, token: &T) {
        match self.items.get(self.index + 1) {
            Some(_) => (),
            None => self.items.push(EarleySet { values: Vec::new() }),
        }

        let mut i = 0;

        while i < self.items[self.index].values.len() {
            if self.items[self.index].values[i].dot
                == self.items[self.index].values[i].rule.rhs.len()
            {
                // Complete
                let nt = &self.items[self.index].values[i].rule.lhs.lhs();

                let mut j = 0;
                let start = self.items[self.index].values[i].start;

                while j < self.items[start].values.len() {
                    let x = &self.items[start].values[j];
                    if let Some(Symbol::Nonterminal(v)) = x.rule.rhs.get(x.dot)
                    {
                        if v == nt {
                            let new_item = EarleyItem {
                                rule: x.rule,
                                dot: x.dot + 1,
                                start: x.start,
                            };
                            self.items[self.index].append(new_item);
                        }
                    }

                    j += 1;
                }
            } else if let Symbol::Nonterminal(nt) =
                &self.items[self.index].values[i].rule.rhs
                    [self.items[self.index].values[i].dot]
            {
                // Predict
                if self.nullable.contains(nt) {
                    let new_item = EarleyItem {
                        rule: self.items[self.index].values[i].rule,
                        dot: self.items[self.index].values[i].dot + 1,
                        start: self.items[self.index].values[i].start,
                    };
                    self.items[self.index].append(new_item);
                }

                // Lookahead
                if let Some(xs) = self.first_set.get(nt) {
                    if !xs.contains(&token) {
                        continue;
                    }
                }

                for r in
                    self.predict_lut.entry(nt.to_string()).or_insert_with(Vec::new).iter()
                {
                    let new_item = EarleyItem {
                        rule: r,
                        dot: 0,
                        start: self.index,
                    };
                    self.items[self.index].append(new_item);
                }
            } else if let Symbol::Terminal(t) =
                &self.items[self.index].values[i].rule.rhs
                    [self.items[self.index].values[i].dot]
            {
                // Scan
                if t == token {
                    let new_item = EarleyItem {
                        rule: self.items[self.index].values[i].rule,
                        dot: self.items[self.index].values[i].dot + 1,
                        start: self.items[self.index].values[i].start,
                    };
                    self.items[self.index + 1].append(new_item);
                }
            }

            i += 1;
        }

        self.index += 1;

        // Handle errors
        if self.items[self.index].values.is_empty() {
            println!("FAILURE IN EARLEY PARSER");
            process::exit(0);
        }
    }
}
