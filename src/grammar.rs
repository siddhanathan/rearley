
#[derive(Debug, PartialEq, Clone)]
pub enum Symbol<T> {
    Nonterminal(String),
    Terminal(T),
}

impl<T> Symbol<T> {
    pub fn lhs(&self) -> String {
        match self {
            Symbol::Nonterminal(x) => x.clone(),
            Symbol::Terminal(_) => unimplemented!(),
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Rule<T> {
    pub lhs: Symbol<T>,
    pub rhs: Vec<Symbol<T>>,
}

pub type Grammar<T> = Vec<Rule<T>>;
