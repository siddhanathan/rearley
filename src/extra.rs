
// #[derive(Debug, PartialEq, Clone)]
// pub struct ParseTree<'a, T> {
//     pub start: usize,
//     pub stop: usize,
//     pub rule: Option<&'a Rule<'a, T>>,
//     pub children: Vec<ParseTree<'a, T>>,
//     pub node: Option<&'a Symbol<'a, T>>,
// }
// 
// pub fn extract<'a, T>(
//     grammar: &'a [Rule<'a, T>],
//     chart: Chart<'a, T>,
// ) -> ParseTree<'a, T> {
//     fn fill_children<'a, T>(
//         root: &mut ParseTree<'a, T>,
//         grammar: &'a [Rule<'a, T>],
//         chart: Chart<'a, T>,
//     ) -> Vec<ParseTree<'a, T>> {
//         let mut start = root.start;
//         let mut stop = root.stop;
// 
//         for i in (0..root.rule.unwrap().rhs.len()).rev() {
//             match &root.rule.unwrap().rhs[i] {
//                 Symbol::Terminal(x) => {
//                     root.children[i] = ParseTree {
//                         start: stop,
//                         stop: stop,
//                         rule: None,
//                         children: Vec::with_capacity(0),
//                         node: Some(&root.rule.unwrap().rhs[i]),
//                     };
//                     stop -= 1;
//                 }
//                 Symbol::Nonterminal(x) => {
//                     for y in chart.items[stop + 1].values.iter() {
//                         if &y.rule.lhs.lhs() == x && y.start >= root.start {
//                             if y.start != start {
//                                 continue;
//                             }
//                         }
//                     }
//                 }
//             }
//         }
//         unimplemented!();
//     }
// 
//     if chart.items.last().unwrap().values.is_empty() {
//         println!("[2] FAILURE IN EARLEY PARSER");
//         process::exit(0);
//     }
// 
//     let mut root = ParseTree {
//         start: 0,
//         stop: chart.items.len() - 1,
//         rule: Some(&grammar[0]),
//         children: Vec::with_capacity(grammar[0].rhs.len()),
//         node: Some(&grammar[0].lhs),
//     };
// 
//     fill_children(&mut root, grammar, chart);
// 
//     root
// }

